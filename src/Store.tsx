import { ref, set as setFire, onValue } from "firebase/database";
import { create } from 'zustand';

import { db } from "./firebase";


export const useStatStore = create(set => ({
  setItem: (key: string, value: any) => set(state => ({stat: {...state.stat, [key]: value}})),
  stat: null,
}));


export const useDlgStore = create(set => ({
  error: null,
  msg: null,

  setError: error => set({ error }),
  setMsg: msg => set({ msg }),
}));


export const useBitStore = create(set => ({

  contract: null,
  setContract: contract => set({ contract }),

  bits: {},

  selectionType: "own",
  maxSelected: 2,
  selected: [],

  toUpdate: new Set(),

  ownFilter: null,
  allFilter: null,
  
  add: (number, data) => {
    set(state => {
      const bitRef = ref(db, `${state.contract}/bits/${number}`);

      setFire(bitRef, data).catch(error => {
        console.error("Error writing to Firebase:", error);
      });

      return { bits: { ...state.bits, [number]: data } };
    });
  },

  remove: (number) => {
    set(state => {
      const bitRef = ref(db, `${state.contract}/bits/${number}`);

      setFire(bitRef, null).catch(error => {
        console.error("Error writing to Firebase:", error);
      });

      const updatedBits = { ...state.bits };
      delete updatedBits[number];

      return { bits: updatedBits };
    })

    },

  clearBits: () => set({ bits: {} }),
  clearSelection: () => set({ selected: [] }),

  setMaxSelected: max => set({ maxSelected: max }),
  setSelectionType: type => set({ selectionType: type }),

  select: bit => set(state => {
    if (state.selected.includes(bit)) {
      return { selected: state.selected.filter(b => b !== bit) };
    }

    if (state.selected.length < state.maxSelected) {
      return { selected: [...state.selected, bit] };
    }

    const newSelection = [...state.selected];
    newSelection.shift();
    newSelection.push(bit);

    return { selected: newSelection };
  }),

  updateBit: bit => set(state => {
    const newSet = new Set(state.toUpdate);
    newSet.add(bit);

    return {toUpdate: newSet }
  }),

  setBitUpdated: bit => set(state => {
    const newSet = new Set(state.toUpdate);
    newSet.delete(bit);

    return {toUpdate: newSet}
  }),

  setOwnFilter: filter => set({ ownFilter: filter }),
  setAllFilter: filter => set({ allFilter: filter }),

  isExternalUpdate: false,

  updateFromFirebase: (newData) => {
    set(state => {
      if (state.isExternalUpdate) {
        return { bits: newData, isExternalUpdate: false };
      }
      return state;
    });
  },

  initializeFromFirebase: () => {
    const bitsRef = ref(db, 'bits');

    onValue(bitsRef, (snapshot) => {
      const data = snapshot.val();

      if (data) {
        useBitStore.getState().updateFromFirebase(data);
        useBitStore.setState({ isExternalUpdate: true });
      }
    });
  }

}));
