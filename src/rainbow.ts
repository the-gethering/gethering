import { darkTheme } from "@rainbow-me/rainbowkit";


export const rainbowConfig = {
  theme: darkTheme({
    accentColor: "black",
    borderRadius: "none",
    overlayBlur: "none",
    fonts: {
      body: 'PixelGosub',
    },
  }),
}
