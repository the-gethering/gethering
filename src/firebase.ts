import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";


const firebaseConfig = {
  apiKey: "AIzaSyD73sLm7avhgNKrgZBuNkJhdccAKg37G3s",
  authDomain: "gethering-701ec.firebaseapp.com",
  databaseURL: "https://gethering-701ec-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "gethering-701ec",
  storageBucket: "gethering-701ec.appspot.com",
  messagingSenderId: "445067505471",
  appId: "1:445067505471:web:f8940026cac318d48b6d8a",
  measurementId: "G-39N3J4B52P"
};


export const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);
