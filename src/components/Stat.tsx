import { useEffect } from "react";

import { useNetwork, useAccount, useBalance, useFeeData, useBlockNumber } from "wagmi";

import { useStatStore, useBitStore } from "../Store"

import {
  useGetheringBalanceOf,
  useGetheringMintedNumber,
  useGetheringTotalNumber,
  useGetheringMintSize,
  useGetheringMaxSize,
  useGetheringOwnerOf,
  useGetheringTokenTtl,
  useGetheringPercentageIncreasePerBlock,
  useGetheringPercentageMultiplier,
  useGetheringBasePercentage,
  useGetheringRoundNumber,
  getheringAddress,
} from "../contracts";


const StatItem = ({center, title, children}) => {
  return <div title={title} className={`cursor-pointer ${center ? 'w-full w-justify mb-4 sm:m-2' : 'w-1/2'} md:w-auto pr-2 md:pr-0 text-center`}>
    {children}
  </div>
} 


export const Stat = () => {

  const { chain } = useNetwork();
  const { address } = useAccount();
  const { stat, setItem } = useStatStore();
  const { contract, setContract } = useBitStore();

  const { data: trethury, isError, isLoading } = useBalance({
    onSuccess: data => setItem('trethury', parseInt(data?.formatted || 0)),
    address: getheringAddress[chain.id],
    watch: true,
  })

  useEffect(() => {
    setContract(getheringAddress[chain.id]);
  }, [chain])

  useGetheringTotalNumber({
    onSuccess: data => setItem('total', parseInt(data)),
    watch: true,
  });

  useGetheringMintedNumber({
    onSuccess: data => setItem('minted', parseInt(data)),
    watch: true,
  });

  useGetheringBalanceOf({
    args: [address!],
    onSuccess: data => setItem('balance', parseInt(data)),
    watch: true,
  });

  useGetheringMintSize({
    onSuccess: data => setItem('mintSize', parseInt(data)),
  });

  useGetheringMaxSize({
    onSuccess: data => setItem('maxSize', parseInt(data)),
  });

  useGetheringTokenTtl({
    onSuccess: data => setItem('tokenTTL', parseInt(data)),
  });

  useGetheringPercentageIncreasePerBlock({
    onSuccess: data => setItem('percentageIncrease', parseInt(data)),
  });

  useGetheringPercentageMultiplier({
    onSuccess: data => setItem('percentageMultiplier', parseInt(data)),
  });

  useGetheringBasePercentage({
    onSuccess: data => setItem('basePercentage', parseInt(data)),
  });

  useGetheringRoundNumber({
    onSuccess: data => setItem('roundNumber', parseInt(data)),
  });

  useBlockNumber({
    onBlock: data => setItem('block', parseInt(data)),
  });

  useFeeData({
    onSuccess: data => setItem('feeData', data),
    watch: true,
  });

  useGetheringOwnerOf({
    enabled: !!(stat?.total && stat?.maxSize === stat?.total),
    args: [stat?.roundNumber * stat?.maxSize],
    onSuccess: data => {
      setItem('winner', data);
    },
  });

  return <div className="flex flex-wrap items-center justify-center h-auto sm:h-20 font-pixel w-full px-12 py-4 sm:p-0 sm:space-x-8">
    <StatItem center={true} title="The amount of ETH a winner grabs">TrETHury: {trethury?.formatted} {trethury?.symbol}</StatItem>
    <StatItem title="A number of bits out there">Total: {stat?.total}</StatItem>
    <StatItem title="Bits that have been minted so far">Minted: {stat?.minted}</StatItem>
    <StatItem title="Bits that have been gETHered so far">Gethered: {stat?.total - stat?.minted}</StatItem>
    <StatItem title="Bits you possess">Balance: {stat?.balance}</StatItem>
  </div>
}
