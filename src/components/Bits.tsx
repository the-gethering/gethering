import { useState, useRef, useEffect } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination, Keyboard } from 'swiper/modules';

import 'swiper/css/bundle';

import { useAccount } from "wagmi";

import { useStatStore, useBitStore } from "../Store";
import { Bit, Collapsible } from "../components";

import { useGetheringTokensOfOwner } from "../contracts"


export function Empty() {
  return <>
    <div className="flex justify-center text-3xl font-bold cursor-pointer mt-10" title="Mint one!">
      ¯\_(ツ)_/¯
    </div>
  </>
}


export function Bits({bitIds}) {
  const [swiper, setSwiper] = useState();

  const spaceBetweenSlides = 5;

  const slidesPerView = (swiper && swiper.params) ? swiper.slidesPerViewDynamic?.() : 1;
  const slidesFit = slidesPerView >= bitIds?.length || slidesPerView === 1;
  const slidesOffset = slidesFit ? 0 : 64;

  const onSwiper = swiper => {
    setSwiper(swiper); 
  }

  return <div className="flex justify-center">
    <Swiper className={`w-full md:w-3/4 h-40 ${slidesOffset > 0 ? "swiper-faded" : ""}`}

            freeMode={"sticky"}
            slidesPerView={"auto"}
            centeredSlides={false}
            centerInsufficientSlides={true}

            slidesOffsetBefore={slidesOffset}
            slidesOffsetAfter={slidesOffset}
            spaceBetween={spaceBetweenSlides}

            modules={[Keyboard, Pagination]}
            keyboard={{enabled: true}}
            pagination={{
              clickable: true,
              dynamicBullets: true,
            }}

            onSwiper={onSwiper}>
      {
         bitIds?.map((n, i) => (
           <SwiperSlide key={n}>
             <Bit bitId={n} />
           </SwiperSlide>
         ))
      }
    </Swiper>
  </div>;
}


const filters = {
  gethered: data => data.getheredTimes > 0  
}


export function OwnBits() {
  const {address} = useAccount();

  const { data, isLoading } = useGetheringTokensOfOwner({
    args: [address],
    watch: true,
  }); 

  const bitIds = data || [];

  return <>
    <div className="own-bits">
      <Collapsible open={true} header="Own bits" title="Your collection">
        { bitIds.length ? <Bits bitIds={[...bitIds].reverse()} /> : <Empty /> }
      </Collapsible>
    </div>
  </>

}


export function AllBits() {
  const { stat } = useStatStore();
  const { bits, allFilter, setAllFilter } = useBitStore();

  const firstBitId = stat?.maxSize * (stat?.roundNumber - 1);
  const lastBitId = firstBitId + stat?.total; 

  const bitIds = firstBitId >= 0 && lastBitId >= 0 ? Array(stat?.total || 0).fill(null)
    .map((_, i) => lastBitId - i)
    .filter((bitId) => {
      const data = bits[bitId];
      const isActive = data ? !data.burned : true;

      if (allFilter && allFilter !== "active") {
        return filters[allFilter] && isActive 
      }

      return isActive 
    }) : [];

  return <>
    <div className="all-bits">
      <Collapsible open={true} header="All bits" title="Explore others" onOpen={() => setAllFilter("active")}>
        { bitIds.length ? <Bits bitIds={bitIds} /> : <Empty /> }
      </Collapsible>
    </div>
  </>
}
