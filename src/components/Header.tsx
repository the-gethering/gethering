import { ConnectButton } from "@rainbow-me/rainbowkit";
import { Links } from "./Links";


const Logo = () => {
  return <div className="font-pixel mx-5 cursor-pointer" title="The great gathering!">
    <span className="text-gray-700 text-6xl sm:text-5xl">G</span>
    <span className="text-gray-900 font-bold text-7xl sm:text-6xl">ETH</span>
    <span className="text-gray-700 text-6xl sm:text-5xl">ERING</span>
  </div>
}


export const Header = () => {
  return <div className="flex flex-col items-center justify-start w-full sm:flex-row sm:justify-between p-2">
    <div className="w-full text-center sm:text-left">
      <Logo />
    </div>

    <div className="order-2 sm:order-1 mt-4 w-full sm:mt-4 sm:absolute sm:left-1/2 sm:transform sm:-translate-x-1/2 top-4">
      <div className="w-full text-center sm:text-left">
        <Links />
      </div>
    </div>

    <div className="order-1 m-4 sm:m-0 sm:order-2 sm:absolute sm:top-7 sm:right-6">
      <ConnectButton className="font-pixel" label="Sign In" accountStatus="address" chainStatus="icon" />
    </div>
  </div>
}
