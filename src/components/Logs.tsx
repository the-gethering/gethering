
import { useNetwork, useContractEvent } from 'wagmi';

import { Collapsible } from "../components";

import { getheringABI, getheringAddress } from "../contracts";


export const Logs = ({}) => {

  const {chain} = useNetwork();

  useContractEvent({
    address: getheringAddress[chain.id],
    abi: getheringABI,
    eventName: "TokenMinted",
    listener(owner, tokenId) {
      console.log(owner, tokenId)
    },
  })

  return <>
    <Collapsible header="Activity" title="Observe the action">
      <div className="text-center">
        <i>Soonish...</i>
      </div>
    </Collapsible>
  </>
}
