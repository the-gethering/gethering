import { useState, useEffect } from "react";

import { useAccount } from "wagmi";
import { parseEther, parseGwei, parseUnits, formatEther } from "viem";

import { Bits, Button } from "../components";
import { Modal, ETHInput, calculateMinimumPrice } from "../components/Utils";

import { useStatStore, useBitStore, useDlgStore } from "../Store";

import {
  useGetheringMint,
  useGetheringGether,
  usePrepareGetheringPutTokenForSale,
  usePrepareGetheringSetTokenPrice,
  usePrepareGetheringRemoveTokenFromSale,
  usePrepareGetheringBuyToken,
  useGetheringPutTokenForSale,
  useGetheringSetTokenPrice,
  useGetheringMinimumPrice,
  useGetheringRemoveTokenFromSale,
  useGetheringBuyToken,
  useGetheringClaim,
  getheringConfig,
} from "../contracts";


const MintButton = ({disabled}) => {

  const { clearSelection } = useBitStore();

  const params = {

    onSettled: () => {
      clearSelection();
    }

  }

  return <>
    <Button hook={useGetheringMint} disabled={disabled} params={params} title="Get your own bit!">Mint</Button>
  </>
}


const GetherButton = ({disabled}) => {

  const { selected, clearSelection, updateBit } = useBitStore();

  const params = {
    args: selected,

    onReceipt: data => {
      const [bitId1, bitId2] = selected;

      updateBit(bitId1);
      updateBit(bitId2);

      clearSelection();
    }
  };

  return <>
    <Button hook={useGetheringGether} disabled={disabled} params={params} title="Make a new one!">Gether</Button>
  </>
}


const BuyButton = ({bitId, bit, sale, disabled}) => {

  const { stat } = useStatStore();
  const { setError } = useDlgStore();
  const { clearSelection, updateBit } = useBitStore();

  const [trigger, setTrigger] = useState(false);

  const bitPrice = BigInt((sale ? bit?.boughtPrice : bit?.price) || 0);

  const maxGas = 32_000n; 
  const gasPrice = stat?.feeData?.gasPrice || 0n;
  const gasCost = maxGas * gasPrice;

  const { config, isFetched } = usePrepareGetheringBuyToken({
    args: [bitId],
    value: bitPrice > 0 ? bitPrice + 1n * gasCost : 0,
    enabled: trigger && gasCost,

    onSettled: (data, error) => {
      setTrigger(false);
      error && setError(error?.message);
    },
  })

  const prepare = () => {
    setTrigger(true);
    return true
  }

  const buyParams = {
    ...config,

    onSettled: () => {
      setTrigger(false);
      clearSelection();
    },

    onReceipt: data => {
      updateBit(bitId);
    }

  };

  return <>
    <Button hook={useGetheringBuyToken} prepare={prepare} ready={isFetched} trigger={trigger}
            disabled={disabled} params={buyParams} title="Grab more!">
      Buy
    </Button>
  </>
}


const SellButton = ({bitId, bit, sale, disabled}) => {

  const { setError } = useDlgStore();
  const { selected, selectionType, updateBit } = useBitStore();

  const [sellPrice, setSellPrice] = useState(0);
  const [sellAction, setSellAction] = useState("put");
  const [showSellDlg, setShowSellDlg] = useState(false);

  const [canSell, setCanSell] = useState(false);
  const [trigger, setTrigger] = useState(false);

  const sellActions = {
    put: {
      prep: usePrepareGetheringPutTokenForSale,
      hook: useGetheringPutTokenForSale,
      title: "sell",
    },
    set: {
      prep: usePrepareGetheringSetTokenPrice,
      hook: useGetheringSetTokenPrice,
      title: "edit",
    },
    remove: {
      prep: usePrepareGetheringRemoveTokenFromSale,
      hook: useGetheringRemoveTokenFromSale,
      title: "unlist",
    }
  }

  const { config, isFetched } = sellActions[sellAction]["prep"]({
    args: sellAction === "remove" ? [bitId] : [bitId, sellPrice],
    enabled: trigger,

    onSettled: (data, error) => {
      setTrigger(false);
      error && setError(error?.message);
    },
  });

  const prepare = () => {
    setShowSellDlg(true);
    return true
  }

  const resetSell = () => {
    setSellPrice(0n);
  }

  const confirmSellAction = () => {
    setTrigger(true);
    setShowSellDlg(false);
  }

  const cancelSellAction = () => {
    setShowSellDlg(false);
    resetSell();
  }

  const params = {
    ...config,

    onSettled: (data, error) => {
      setTrigger(false);
      resetSell();
    },

    onReceipt: data => {
      updateBit(bitId);
    },
  };

  useEffect(() => {
    const handleKeyDown = (event) => {
      if (event.key === 'Enter') {
        (sellPrice || sellAction === "remove") && confirmSellAction();
      } else if (event.key === 'Escape') {
        cancelSellAction();
      }
    }

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    }
  }, [sellPrice, sellAction]);

  useEffect(() => {
    if (selectionType === "own") {
      setSellAction(selected.length === 1 && bit?.price > 0 ? "set" : "put");
    }
  }, [selected, bit]);

  const {data: minSellPrice} = useGetheringMinimumPrice({
    args: [bitId, bit?.boughtPrice],
    enabled: showSellDlg,
    watch: true,
  })

  const sellDlg = (currentPrice) => {
    const btnClasses = "text-white text-3xl font-pixel p-4 pl-5 pb-4 py-2 mx-5 mt-5 hover:bg-white hover:text-black disabled:bg-opacity-30";

    const setValue = value => {
      const price = parseEther(value)

      if (price) {
        setSellPrice(price);
        setCanSell(true);
      } else {
        setCanSell(false);
      }
    }

    const onChange = value => {
      const price = parseEther(value);
      
      if (sellAction === "set" && price === 0n) {
        setSellAction("remove");
      }

      if (sellAction === "remove" && price > 0n) {
        setSellAction("set");
      }
    }

    const value = formatEther({
      "put": minSellPrice > sellPrice ? minSellPrice : sellPrice,
      "set": minSellPrice > sellPrice ? minSellPrice : sellPrice || currentPrice,
      "remove": 0n,
    }[sellAction]).slice(0, 8);

    const disabled = {
      "put": !canSell,
      "set": !currentPrice && !canSell,
      "remove": value > 0n,
    }[sellAction];

    return <Modal>
      <div className="flex flex-col items-center font-pixel"> 
        <div className="flex-2 w-full text-center">
          <ETHInput value={value} setValue={setValue} onChange={onChange} />
        </div>
        <div className="flex items-center">
          <div className="flex-1 text-center">
            <button onClick={confirmSellAction}
                    className={btnClasses}
                    disabled={disabled}>
              {sellActions[sellAction]["title"]}              
            </button>
          </div>

          <div className="flex-1 text-center">
            <button onClick={cancelSellAction} className={btnClasses}>
              cancel
            </button>
          </div>
        </div>
      </div>
    </Modal>
  };

  return <>
    <Button hook={sellActions[sellAction]["hook"]} prepare={prepare} ready={isFetched} trigger={trigger}
            disabled={disabled} params={params} title="Trade it!">
      {sellActions[sellAction]["title"]}
    </Button>

    { showSellDlg && sellDlg(bit?.price) }
  </>
}


const ClaimButton = () => {

  const { setItem } = useStatStore();
  const { setMsg } = useDlgStore(); 

  const classes = `
    w-32 h-32 bg-gradient-to-r from-yellow-300 to-yellow-500 text-gray-800 font-semibold flex items-center justify-center
    hover:bg-yellow-600 hover:text-grey-800 shadow-lg hover:shadow-xl transform hover:scale-105 transition-all duration-300 animate-pulse
  `;

  const params = {
    onReceipt: data => {
      setMsg(`Congrats, you won the gETHering and gETHered your reward!`);
      setItem("winner", null);
    }
  }

  return <>
    <div>
      <Button hook={useGetheringClaim} params={params} title="You won!!!" className={classes}>Claim</Button>
    </div>
  </>
}


export const Buttons = () => {

  const { address } = useAccount();

  const { stat } = useStatStore();
  const { bits, selected, selectionType, updateBit } = useBitStore();

  const [bitId, ..._] = selected;

  const bit = bits[bitId];
  const sale = stat?.block > bit?.mintingBlock + stat?.tokenTTL;

  const isMintDisabled = stat?.mintSize === stat?.minted;
  const isGetherDisabled = !(selectionType === "own" && selected.length === 2);
  const isSellDisabled = !(selectionType === "own" && !sale && selected.length === 1);
  const isBuyDisabled = !(selectionType === "all" && selected.length === 1);

  return (
    <div className="flex flex-wrap items-center justify-center px-10 sm:p-0">
      { 
        stat?.winner === address ? 
          <div>
            <ClaimButton />
          </div>
            :
          <>
            <MintButton disabled={isMintDisabled} />
            <GetherButton disabled={isGetherDisabled} />

            <SellButton bitId={bitId} bit={bit} sale={sale} disabled={isSellDisabled} />
            <BuyButton bitId={bitId} bit={bit} sale={sale} disabled={isBuyDisabled} />
          </>
      }
    </div>
  );
}
