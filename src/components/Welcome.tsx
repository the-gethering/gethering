import { useConnectModal } from '@rainbow-me/rainbowkit';


export const Welcome = () => {

  const { openConnectModal } = useConnectModal(); 

  return <div className="flex items-center justify-center flex-grow">
    <div className="font-pixel text-center ">
      <div className="text-4xl">A puzzling bit surely rewarding</div>
      <div className="font-bold text-6xl"><a href="#" onClick={openConnectModal}>Gether in!</a></div>
    </div>
  </div>
}
