import { useState, useEffect } from "react";
import { useNetwork, useWaitForTransaction } from "wagmi";

import { Loader, Modal } from "../components/Utils";
import { useStatStore, useBitStore, useDlgStore } from "../Store";


export const Button = ({hook, prepare, ready, trigger, params, disabled, title, children, className}) => {

  const { setError } = useDlgStore();

  const {data, write, isLoading} = hook({
    ...(params || {}),
    onSettled: (data, error) => {
      params.onSettled?.(data, error);
      error && setError(error?.message);
      setTriggered(false);
    },
  });

  const {isLoading: isTxLoading} = useWaitForTransaction({
    confirmations: 1,
    hash: data?.hash,

    onSuccess: data => {
      params.onReceipt?.(data);
    }
  });

  const { chain } = useNetwork();

  const isDisabled = isLoading || disabled;
  const isBusy = isLoading || isTxLoading;

  const baseClasses = "bg-gray-500 text-white font-bold w-32 h-32 m-1 shadow-md shadow-gray-500";
  const disabledClasses = "opacity-60 cursor-not-allowed";
  const enabledClasses = "hover:bg-gray-700";

  const etherscan = chain?.blockExplorers?.etherscan;

  const tx = (
    etherscan &&
      <a href={`${etherscan.url}/tx/${data?.hash}`} className={data?.hash ? "text-white" : "text-transparent"} target="_blank">
          Check tx on {etherscan.name}
      </a>
  )

  const [triggered, setTriggered] = useState(false);

  useEffect(() => {
    if (trigger && !triggered && ready && write) {
      write();

      setTriggered(true);
    }
  })

  const onClick = () => {
    prepare?.() || write?.();
  }

  return <div className="md:w-auto p-1 px-1 box-border">
    <button title={title} disabled={isDisabled} onClick={onClick}
            className={`${className} ${baseClasses} ${isDisabled ? disabledClasses : enabledClasses }`}>
      <div className="font-pixel">{children}</div>
    </button>

    { 
      isBusy &&
        <Modal>
          <div className="flex flex-col items-center">
            <div className="flex items-center">
              <div className="flex-1 text-center">
                <div className="text-white text-2xl font-pixel">
                  {children}ing
                </div>
              </div>

              <div className="flex-1 text-center">
                <Loader />
              </div>
            </div>

            <div className="flex-2 w-full text-center">
              <div className="text-sm font-pixel">
                {tx}
              </div>
            </div>
          </div>
        </Modal>
    }
  </div>
}
