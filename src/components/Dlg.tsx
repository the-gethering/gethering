import { useEffect } from "react";

import { Modal } from "../components";

import { useDlgStore } from "../Store";


export const ErrorDlg = () => {
  const {error, setError} = useDlgStore(); 

  let txt = error || "Error :(";

  if (error) {
    [
      /following reason:\n(.*?)\n/s,
      /(The total cost (.+?) exceeds the balance of the account)/,
      /(User rejected the request)/,
      /(Price cannot (.+?) bought for)/,
      /(Execution reverted for an unknown reason)/,
      /(Invalid UserOp signature or paymaster signature)/,
      /(RPC Error)/,
    ].forEach(template => {
      const matches = error.match(template);
      
      if (matches && matches[1]) {
        txt = matches[1].trim();
      } 
    })
  }

  useEffect(() => {
    error && setTimeout(() => setError(null), 3000);
  }, [error])

  return <>
    {
      error && 
        <Modal>
          <div className="text-center text-white font-pixel text-3xl">
            {txt}
          </div>
        </Modal>
    }
  </>
}


export const MsgDlg = () => {

  const {msg, setMsg} = useDlgStore(); 

  useEffect(() => {
    msg && setTimeout(() => setMsg(null), 3000);
  }, [msg])

  return <>
    {
      msg && 
        <Modal>
          <div className="text-center text-white font-pixel text-3xl">
            {msg}
          </div>
        </Modal>
    }
  </>
}
