import { useState, useEffect } from 'react';


export const Loader = () => {
  return <>
    <span className="loader flex space-x-2 items-end mx-1 h-6">
      <span className="dot bg-white w-1 h-1"></span>
      <span className="dot bg-white w-1 h-1"></span>
      <span className="dot bg-white w-1 h-1"></span>
    </span>
  </>
}


export const Modal = ({ children }) => {
  return <>
    <div className="fixed inset-0 bg-black bg-opacity-90 flex items-center justify-center z-50">
      { children }
    </div>
  </> 
}


export const ETHInput = ({value, setValue, onChange}) => {

  const handleInput = (e) => {
      const val = (e.target.value || "0");
      const decimalCount = (val.match(/\./g) || []).length;

      if (!/^[0-9]*\.?[0-9]*$/.test(val) && val !== "0" || decimalCount > 1) {
        e.target.value = val.substring(0, val.length - 1);
      } else {
        setValue(val);
      }

      onChange(val);
  };

  return (
    <div className="text-center">
      <input
        autoFocus
        type="text"
        value={value}
        onInput={handleInput}
        placeholder="Set the price"
        className="p-3 pb-5 w-3/4 text-4xl text-center bg-black bg-opacity-0 text-white focus:border-white outline-none focus:ring focus:ring-white focus:ring-opacity-10 placeholder-gray-600 caret-gray-300"
      />
    </div>
  );
};


export const Collapsible = ({ header, title, open, onOpen, children }) => {
  const [isOpen, setIsOpen] = useState(open);

  const handleClick = () => {
    setIsOpen(!isOpen);
    onOpen?.(isOpen);
  }

  return (
    <div className="w-full font-pixel">
      <button className="group p-4 text-gray-800 w-full md:w-auto" onClick={handleClick} title={title}>
        <div className="flex items-center justify-between w-full">
          <span className="flex items-center justify-center font-bold">
            {header}
          </span>
          <span className="ml-2 mt-0.5">
            {isOpen ? (
              <div className="md:group-hover:block md:hidden w-3 h-1">
                <div className="w-full h-1 bg-black"></div>
              </div>
            ) : (
              <div className="md:group-hover:block md:hidden relative w-3 h-3">
                <div className="absolute top-1/2 left-0 w-full h-1 bg-black transform -translate-y-1/2"></div>
                <div className="absolute top-0 left-1/2 w-1 h-full bg-black transform -translate-x-1/2"></div>
              </div>
            )}
          </span>
        </div>
      </button>

      <div className={`${isOpen ? 'opacity-100' : 'max-h-0 opacity-0'} transition-all duration-500 ease-in-out overflow-hidden border-t border-black`}>
        {children}
      </div>
    </div>
  );
}


export const Timer = ({percentage}) => {
  const fraction = percentage / 100;

  const getClipPath = (fraction) => {
    const radian = 2 * Math.PI * fraction;
    const halfDiagonal = Math.sqrt(2 * 50 * 50);
    const x = 50 + halfDiagonal * Math.sin(radian);
    const y = 50 - halfDiagonal * Math.cos(radian);

    const base = 'polygon(50% 50%, 50% 0%, 100% 0%,';
    const vars = ` ${x}% ${y}%)`;

    if (fraction <= 0.25) {
      return `${base} ${vars}`;
    } else if (fraction <= 0.5) {
      return `${base} 100% 100%, ${vars}`;
    } else if (fraction <= 0.75) {
      return `${base} 100% 100%, 0% 100%, ${vars}`;
    } else {
      return `${base} 100% 100%, 0% 100%, 0% 0%, ${vars}`;
    } 
  };

  return (
    <div style={{ clipPath: getClipPath(fraction) }}
         className={`absolute top-0 left-0 w-full h-full inside-out-gradient`}>
    </div>
  );
};


export const percentageAtBlock = (
  mintingBlock, targetBlock, 
  TOKEN_TTL, PERCENTAGE_INCREASE_PER_BLOCK,
  PERCENTAGE_MULTIPLIER, BASE_PERCENTAGE, 
) => {
  return (() => {
    const blocksDelta = targetBlock - mintingBlock;

    if (blocksDelta > TOKEN_TTL) {
        return PERCENTAGE_MULTIPLIER;
    }

    const percentage = BASE_PERCENTAGE + blocksDelta * PERCENTAGE_INCREASE_PER_BLOCK;

    if (percentage >= PERCENTAGE_MULTIPLIER) {
        return PERCENTAGE_MULTIPLIER;
    }

    return percentage;
  })() / PERCENTAGE_MULTIPLIER * 100;
}
