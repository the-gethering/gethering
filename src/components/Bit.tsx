import { useEffect, useState } from "react";
import { formatEther } from "viem";

import moment from 'moment';
import 'moment-duration-format';

import { useStatStore, useBitStore } from "../Store";

import { percentageAtBlock, Timer } from "./Utils";

import {
  useGetheringTokenUri,
} from "../contracts";


const Img = ({ image, className, shadow, children }) => {
  return <div className={`relative ${className}`}>
      {image ? <img className={`w-full h-full ${shadow}`} src={image}/> : ""}
      {children}
  </div>
}


export function Bit({ bitId }) {

  const { stat } = useStatStore();
  const {
    bits, add, remove, clearBits, 
    toUpdate, setBitUpdated,
    selected, select, clearSelection,
    setMaxSelected, selectionType, setSelectionType,
  } = useBitStore();

  const bit = bits[bitId];

  const parse = data => {
    if (data) {
      const value = atob(data.substring(29));
      return JSON.parse(value)
    }

    return { image: "" }
  }

  const { data, refetch } = useGetheringTokenUri({
    args: [bitId],
    onSuccess: data => add(bitId, parse(data)),
  });

  useEffect(() => {
    if (bitId && toUpdate.has(bitId)) {
      setBitUpdated(bitId);
      refetch?.(); 
    }
  }, [bitId, toUpdate]);

  const { image, ...attrs } = bit || parse(data);

  const isSelected = selected.includes(bitId);

  const handleClick = (event) => {
    if (event.target.closest('.own-bits') && selectionType === "all") {
      setSelectionType("own");
      setMaxSelected(2);
      clearSelection();
    }

    if (event.target.closest('.all-bits') && selectionType === "own") {
      setSelectionType("all");
      setMaxSelected(1);
      clearSelection();
    }

    select(bitId);
  }

  const percentage = (stat?.block && bit?.mintingBlock) ? percentageAtBlock(
    bit?.mintingBlock, stat?.block,
    stat?.tokenTTL, stat?.percentageIncrease,
    stat?.percentageMultiplier, stat?.basePercentage
  ) : 0;

  const ttl = stat?.tokenTTL - (stat?.block - bit?.mintingBlock);

  const sale = stat?.block > bit?.mintingBlock + stat?.tokenTTL;
  const price = formatEther(BigInt((sale ? bit?.boughtPrice : attrs?.price) || 0));

  const toTrethury = formatEther(Math.floor(attrs?.price * percentage / 100));

  const baseClasses = "m-1 transition ease-in duration-200 cursor-pointer";
  const selectedClasses = "border-2 border-gray-800 opacity-60";
  const notSelectedClasses = "border-2 border-transparent opacity-100 hover:opacity-80";
  const shadow = !isSelected ? `shadow-${ percentage === 100 ? "lg" : "md"} shadow-gray-${ percentage === 100 ? "500" : "500"}` : "";

  return <div onClick={handleClick} title={ttl > 0 ? `Goes on sale in ${ttl} blocks` : `On sale for Ξ${price.slice(0, 10)}`}>
    <Img image={image} className={`${baseClasses} ${isSelected ? selectedClasses : notSelectedClasses}`} shadow={shadow}>
      { percentage < 100 ? <Timer percentage={percentage} /> : "" }

      { bitId && <div className="absolute text-white white-shadow top-0 ml-2" title="Bit id">#{bitId.toString()}</div> }

      { 
        (bit?.price > 0 || sale) &&
          <div className="absolute text-white white-shadow bottom-1 ml-2 ">
            <div title="Sell price" className="flex">
              <div className="text align-middle mr-1">Ξ</div>
              <div>{ price.slice(0, 10) }</div>
            </div>
            {
              sale ?
                <div title="Sale doesn't add to thrETHury">SALE</div>
                  :
                <div title={`Sale adds Ξ${toTrethury} to the trETHury`} className="flex">
                  <div className="text -ml-1 mr-1">⇧</div>
                  <div>{ toTrethury.slice(0, 10) }</div>
                </div>
            }
          </div>
      }
    </Img>
  </div>
}
