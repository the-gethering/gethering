import { useEffect } from "react";
import { useAccount } from "wagmi";

import { useBitStore } from "./Store";

import { Welcome, Header, Footer, Stat, Buttons, AllBits, OwnBits, Logs, Settings, ErrorDlg, MsgDlg } from "./components";


export function App() {

  const { initializeFromFirebase } = useBitStore();
  const { isConnected } = useAccount();

  useEffect(() => {
    initializeFromFirebase();
  }, []);

  return <div className="flex flex-col min-h-screen">
    {
      isConnected ? 
        <div className="flex flex-col h-screen">
          <div className="bg-gradient-to-b from-gray-300 to-white">
            <Header />
            <Stat/>
            <Buttons/>
          </div>
          <OwnBits /> 
          <AllBits /> 
          <Settings />
          <Logs />
          <Footer />
        </div>
          :
        <div className="flex flex-col h-screen">
          <div className="bg-gradient-to-b from-gray-300 to-white flex flex-col h-screen">
            <Header />
            <Welcome />
            <Footer />
          </div>
        </div>
    }

    <ErrorDlg />
    <MsgDlg />
  </div>
}
