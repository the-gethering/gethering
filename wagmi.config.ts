import { config } from 'dotenv';
import { defineConfig } from "@wagmi/cli";
import { foundry, react } from "@wagmi/cli/plugins";
import * as chains from "wagmi/chains";

import { DEPLOYED_ADDRESS } from "./deployedAddress.ts"


config();

export default defineConfig({
  out: "src/contracts.ts",
  plugins: [
    foundry({
      deployments: {
        Gethering: {
          [chains.base.id]: process.env.GETHERING_BASE,
          [chains.baseGoerli.id]: process.env.GETHERING_BASE_GOERLI,
          //[chains.foundry.id]: DEPLOYED_ADDRESS,
        },
      },
    }),
    react(),
  ],
});
