// SPDX-License-Identifier: MIT

pragma solidity >=0.8.20;


interface IGetheringStruct {

    struct Token {
        uint mask;

        uint roundNumber;

        uint8 getheredTimes;
        uint8 pixelsNumber;

        uint mintingBlock;
        bool burned;

        uint price;
        bool forSale;

        uint boughtPrice;
    }

    struct Round {
        address winner;
        uint reward;

        uint startedBlock;
        uint finishedBlock; 
    }

}
