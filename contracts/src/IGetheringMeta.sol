// SPDX-License-Identifier: MIT

pragma solidity >=0.8.20;

import "./IGetheringStruct.sol";


interface IGetheringMeta is IGetheringStruct {

    function metadataJson(
        uint tokenId, Token calldata token,
        uint MASK_SIZE, uint PIC_SIZE, uint PIXEL_SIZE
    ) external pure returns(string memory metadata);

}
