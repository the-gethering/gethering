// SPDX-License-Identifier: MIT

pragma solidity >=0.8.20;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import "./IGetheringStruct.sol";
import "./IGetheringMeta.sol";


contract Gethering is IGetheringStruct, ERC721Enumerable, Ownable {

    function supportsInterface(bytes4 interfaceId) public view override(ERC721Enumerable) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    IGetheringMeta private gm;

    mapping(uint => uint) private mintMap;
    mapping(uint => Token) private tokenInfo;
    mapping(uint => Round) public roundInfo;

    address public winner;

    uint public totalNumber;
    uint public mintedNumber;
    uint public roundNumber = 1;

    uint private constant POWER = 1;
    uint private constant MASK_SIZE = 2 ** POWER;
    uint public constant MINT_SIZE = MASK_SIZE ** 2;

    uint private constant PIC_SIZE = 1024;
    uint private constant PIXEL_SIZE = PIC_SIZE / MASK_SIZE;

    uint private constant PIC_PIXELS_NUMBER = PIC_SIZE ** 2; 
    uint public constant MAX_SIZE = 2 ** (2 * POWER + 1) - 1;

    uint public constant MINT_LIMIT = 1;
    bool public constant SAFE_PRICE = true;
    bool public constant ADJACENT_GETHERING = true;

    uint public constant TOKEN_TTL = 450;
    uint public constant BASE_PERCENTAGE = 1000;
    uint public constant PERCENTAGE_DECIMAL = 4;
    uint public constant PERCENTAGE_MULTIPLIER = 10 ** PERCENTAGE_DECIMAL;
    uint public constant PERCENTAGE_INCREASE_PER_BLOCK = (PERCENTAGE_MULTIPLIER - BASE_PERCENTAGE) / TOKEN_TTL;

    address public constant MAINTENANCE_ADDRESS = 0x830bc5551e429DDbc4E9Ac78436f8Bf13Eca8434;
    uint public constant MAINTENANCE_PERCENTAGE = 1000;

    event TokenMinted(address indexed owner, uint indexed tokenId);
    event TokensGethered(address indexed owner, uint tokenId1, uint tokenId2, uint indexed newTokenId);
    event TokenBurned(address indexed owner, uint tokenId);
    event TokenForSale(address indexed owner, uint indexed tokenId, uint price);
    event TokenSetPrice(address indexed owner, uint indexed tokenId, uint oldPrice, uint newPrice);
    event TokenRemovedFromSale(address indexed owner, uint indexed tokenId);
    event TokenSold(address indexed oldOwner, address indexed newOwner, uint indexed tokenId, uint price, uint royalty);

    modifier onlyWinner() {
        require(msg.sender == winner, "Not the winner");
        _;
    }

    modifier onlyTokenOwner(uint tokenId) {
        require(ownerOf(tokenId) == msg.sender, "Not the bit owner");
        _;
    }

    modifier onlyTokensOwner(uint tokenId1, uint tokenId2) {
        require(ownerOf(tokenId1) == msg.sender && ownerOf(tokenId2) == msg.sender, "Not the bits owner");
        _;
    }

    modifier onlyActiveToken(uint tokenId) {
        require(isActive(tokenId), "Bit is too old");
        _;
    }

    constructor(address _gmAddress) ERC721("Gethering", "GTHRNG") {
        gm = IGetheringMeta(_gmAddress);

        randomizeMintMap();
        renounceOwnership();
    }

    receive() external payable {}
    fallback() external payable {}

    function _beforeTokenTransfer(address from, address to, uint firstTokenId, uint batchSize) internal override(ERC721Enumerable) {
        super._beforeTokenTransfer(from, to, firstTokenId, batchSize);
    }

    function safeTransferFrom(address, address, uint) public virtual override(ERC721, IERC721) {
        revert("A bit is not directly transferable");
    }

    function safeTransferFrom(address, address, uint, bytes memory) public virtual override(ERC721, IERC721) {
        revert("A bit is not directly transferable");
    }

    function transferFrom(address, address, uint) public pure override(ERC721, IERC721) {
        revert("A bit is not directly transferable");
    }

    function randomizeMintMap() private {
        for (uint i = uint(MINT_SIZE - 1); i > 0; --i) {
            uint j = uint(uint(keccak256(abi.encodePacked(block.timestamp, msg.sender, i))) % (uint(i) + 1));

            uint nextI = mintMap[j] > 0 ? mintMap[j] : j;
            uint nextJ = mintMap[i] > 0 ? mintMap[i] : i;

            if (nextI != nextJ) {
                (mintMap[i], mintMap[j]) = (nextI, nextJ);
            }
        }
    }

    function shiftRoundTokenId(uint roundTokenId) internal view returns (uint) {
        return (roundNumber - 1) * MAX_SIZE + roundTokenId;
    }

    function mint() public {
        require(balanceOf(msg.sender) < MINT_LIMIT, "You have reached the mint limit");
        require(mintedNumber < MINT_SIZE, string(abi.encodePacked("Can't mint more than ", Strings.toString(MINT_SIZE))));

        uint tokenId = shiftRoundTokenId(++totalNumber);

        _mint(msg.sender, tokenId);

        Token memory token;
        token.mask = 1 << (mintMap[uint(mintedNumber++)]);
        token.pixelsNumber = 1;
        token.burned = false;
        token.mintingBlock = block.number;
        tokenInfo[tokenId] = token;

        emit TokenMinted(msg.sender, tokenId);
    }

    function masksAdjucent(uint mask1, uint mask2) internal pure returns (bool) {
        return mask1 << 1 & mask2 != 0
            || mask1 >> 1 & mask2 != 0
            || mask1 << MASK_SIZE & mask2 != 0
            || mask1 >> MASK_SIZE & mask2 != 0
            || mask1 << (MASK_SIZE + 1) & mask2 != 0
            || mask1 << (MASK_SIZE - 1) & mask2 != 0
            || mask1 >> (MASK_SIZE - 1) & mask2 != 0
            || mask1 >> (MASK_SIZE + 1) & mask2 != 0;
    }

    function gether(uint tokenId1, uint tokenId2) external onlyTokensOwner(tokenId1, tokenId2) {
        Token memory token1 = tokenInfo[tokenId1];
        Token memory token2 = tokenInfo[tokenId2];

        if (ADJACENT_GETHERING) {
            require(masksAdjucent(token1.mask, token2.mask), "Can only gether adjucent bits");
        }

        uint tokenId = shiftRoundTokenId(++totalNumber);

        _mint(msg.sender, tokenId);

        Token memory token;
        token.mask = token1.mask | token2.mask;
        token.pixelsNumber = token1.pixelsNumber + token2.pixelsNumber;
        token.getheredTimes = (token1.getheredTimes >= token2.getheredTimes ? token1.getheredTimes : token2.getheredTimes) + 1;
        token.boughtPrice = token1.boughtPrice >= token2.boughtPrice ? token1.boughtPrice : token2.boughtPrice;
        token.mintingBlock = block.number;
        tokenInfo[tokenId] = token;

        burn(tokenId1);
        burn(tokenId2);

        if (tokenId == roundNumber * MAX_SIZE) {
            winner = msg.sender;
        }

        emit TokensGethered(msg.sender, tokenId1, tokenId2, totalNumber);
    }

    function burn(uint tokenId) private {

        _burn(tokenId);

        Token memory token;
        token.burned = true;
        token.forSale = false;
        token.price = 0;

        tokenInfo[tokenId] = token;

        emit TokenBurned(msg.sender, tokenId);
    }

    function claim() external onlyWinner onlyTokenOwner(roundNumber * MAX_SIZE) {
        uint reward = address(this).balance;
        require(reward > 0, "No funds to claim");
      
        uint maintenanceCut = reward * MAINTENANCE_PERCENTAGE / PERCENTAGE_MULTIPLIER;

        (bool successWinner, ) = payable(msg.sender).call{value: reward - maintenanceCut}("");
        (bool successMaintenance, ) = payable(msg.sender).call{value: maintenanceCut}("");
        require(successWinner && successMaintenance, "Transfer failed.");

        Round memory round;
        round.winner = winner;
        round.reward = reward;
        round.finishedBlock = block.number;
        roundInfo[roundNumber] = round;

        burn(roundNumber * MAX_SIZE);

        reset();
    }

    function reset() internal {

        totalNumber = 0;
        mintedNumber = 0;
        winner = address(0);

        roundNumber++;
        roundInfo[roundNumber].startedBlock = block.number;

        randomizeMintMap();
    }

    function tokensOfOwner(address tokenOwner) external view returns (uint[] memory) {
        uint[] memory tokenIds = new uint[](totalNumber);

        uint firstTokenId = shiftRoundTokenId(1);
        uint tokenCount = 0;

        for (uint tokenId = firstTokenId; tokenId < firstTokenId + totalNumber; tokenId++) {
            if (!tokenInfo[tokenId].burned && ownerOf(tokenId) == tokenOwner) {
                tokenIds[tokenCount] = tokenId;
                tokenCount++;
            }
        }

        assembly {
            mstore(tokenIds, tokenCount)
        }

        return tokenIds;
    }

    function isActive(uint tokenId) private view returns (bool) {
        return (block.number - tokenInfo[tokenId].mintingBlock) < TOKEN_TTL;
    }

    function _changePrice(uint tokenId, uint price) internal {

        if (SAFE_PRICE) {
            uint sellerMinReceived = minimumPrice(tokenId, tokenInfo[tokenId].boughtPrice);
            require(price >= sellerMinReceived, "Price cannot be set lower than the bit was bought for");
        }

        tokenInfo[tokenId].price = price;
    }

    function putTokenForSale(uint tokenId, uint price) external onlyTokenOwner(tokenId) onlyActiveToken(tokenId) {
        require(!tokenInfo[tokenId].forSale, "Bit is already for sale");

        _changePrice(tokenId, price);
        tokenInfo[tokenId].forSale = true;

        emit TokenForSale(msg.sender, tokenId, price);
    }

    function setTokenPrice(uint tokenId, uint price) external onlyTokenOwner(tokenId) onlyActiveToken(tokenId) {
        require(tokenInfo[tokenId].forSale, "Bit is not for sale yet");

        uint oldPrice =  tokenInfo[tokenId].price;
        _changePrice(tokenId, price);

        emit TokenSetPrice(msg.sender, tokenId, oldPrice, price);
    }

    function removeTokenFromSale(uint tokenId) external onlyTokenOwner(tokenId) onlyActiveToken(tokenId) {
        tokenInfo[tokenId].forSale = false;
        tokenInfo[tokenId].price = 0;

        emit TokenRemovedFromSale(msg.sender, tokenId);
    }

    function buyToken(uint tokenId) external payable {
        Token memory token = tokenInfo[tokenId];

        address oldOwner = ownerOf(tokenId);
        require(oldOwner != msg.sender, "You are the owner of the bit");

        uint amountToSeller;
        uint royaltyAmount;
        uint salePrice;

        if (isActive(tokenId)) {
            require(token.forSale, "Bit is not for sale");

            salePrice = token.price;

            (, royaltyAmount) = _royaltyInfo(tokenId, salePrice);
            
            amountToSeller = salePrice - royaltyAmount;
        } else {
            salePrice = amountToSeller = token.boughtPrice;
        }

        require(msg.value >= salePrice, "Insufficient funds");
        (bool saleSuccess, ) = payable(oldOwner).call{value: amountToSeller}("");
        require(saleSuccess, "Sale failed");

        uint refundAmount = msg.value - salePrice;

        if (refundAmount > 0) {
            (bool refundSuccess, ) = payable(msg.sender).call{value: refundAmount}("");
             require(refundSuccess, "Refund failed");
        }

        _transfer(oldOwner, msg.sender, tokenId);

        token.price = 0;
        token.forSale = false;
        token.boughtPrice = salePrice;
        token.mintingBlock = block.number;
        tokenInfo[tokenId] = token;

        emit TokenSold(oldOwner, msg.sender, tokenId, salePrice, royaltyAmount);
    }

    function minimumPrice(uint tokenId, uint boughtPrice) public view returns (uint) {
        uint currentPercentage = percentageAtBlock(tokenId, block.number);

        if (currentPercentage == PERCENTAGE_MULTIPLIER) {
            return 0;
        }

        return 2 * boughtPrice * PERCENTAGE_MULTIPLIER / (PERCENTAGE_MULTIPLIER - currentPercentage);
    }

    function percentageAtBlock(uint tokenId, uint targetBlock) public view returns (uint) {
        uint blocksDelta = targetBlock - tokenInfo[tokenId].mintingBlock;

        if (blocksDelta > TOKEN_TTL) {
            return PERCENTAGE_MULTIPLIER;
        }

        uint percentage = BASE_PERCENTAGE + blocksDelta * PERCENTAGE_INCREASE_PER_BLOCK;

        if (percentage > PERCENTAGE_MULTIPLIER) {
            return PERCENTAGE_MULTIPLIER;
        }

        return percentage;
    }

    function royaltyInfo(uint tokenId, uint salePrice) external view returns (address receiver, uint royaltyAmount) {
        return _royaltyInfo(tokenId, salePrice);
    }

    function _royaltyInfo(uint tokenId, uint salePrice) private view returns (address receiver, uint royaltyAmount) {
        return (address(this), (salePrice * percentageAtBlock(tokenId, block.number)) / PERCENTAGE_MULTIPLIER);
    }

    function tokenURI(uint tokenId) public view override(ERC721) returns(string memory) {

        return string(
            abi.encodePacked("data:application/json;base64,",
                gm.metadataJson(
                    tokenId, tokenInfo[tokenId],
                    MASK_SIZE, PIC_SIZE, PIXEL_SIZE
                )
            )
        );
    }

}
