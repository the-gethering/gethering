// SPDX-License-Identifier: MIT

pragma solidity >=0.8.20;

import "@openzeppelin/contracts/utils/Base64.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

import "./IGetheringStruct.sol";


contract GetheringMeta is IGetheringStruct {

    function metadataJson(
        uint tokenId, Token calldata token,
        uint MASK_SIZE, uint PIC_SIZE, uint PIXEL_SIZE
    ) external pure returns(string memory metadata) {
        
        metadata = Base64.encode(
            abi.encodePacked(
                abi.encodePacked(
                    '{', 
                        '  "image": "', getImage(token.mask, 0xff, MASK_SIZE, PIC_SIZE, PIXEL_SIZE), '"',
                        ', "name":"Bit #', Strings.toString(tokenId), '"',
                        ', "description": "A bit of gETHering"',
                        ', "attributes":', attributesJson(token.getheredTimes, token.pixelsNumber)
                ), abi.encodePacked( 
                        ', "roundNumber":', Strings.toString(token.roundNumber),
                        ', "pixelsNumber":', Strings.toString(token.pixelsNumber),
                        ', "getheredTimes":', Strings.toString(token.getheredTimes),
                        ', "mintingBlock":', Strings.toString(token.mintingBlock)
                ), abi.encodePacked(
                        ', "burned":', token.burned ? "true" : "false",
                        ', "price":', Strings.toString(token.price),
                        ', "boughtPrice":', Strings.toString(token.boughtPrice),
                    "}"
                )
            )
        );
    }

    function makeAttribute(string memory key, string memory value) private pure returns (string memory) {
        return string(abi.encodePacked('{"trait_type": "', key, '","value":"', value, '"}'));
    }

    function attributesJson(uint8 getheredTimes, uint8 pixelsNumber) private pure returns(string memory) {
        return string(abi.encodePacked("[",
            makeAttribute("gethered", Strings.toString(getheredTimes)), ",",
            makeAttribute("pixels", Strings.toString(pixelsNumber)),
        "]"));
    }

    function getRandomColor(uint seed, bytes1 alpha) internal pure returns (string memory) {
        bytes1 raw = bytes1(uint8(uint(keccak256(abi.encodePacked(seed))) % 0xbc) + 0x22);
        string memory hexValue = hexEncode(raw);

        return string(abi.encodePacked(hexValue, hexValue, hexValue, hexEncode(alpha)));
    }

    function hexEncode(bytes1 data) internal pure returns (string memory) {
        bytes memory lookup = "0123456789abcdef";
        bytes memory encoded = new bytes(2);

        encoded[0] = lookup[uint8(data) / 16];
        encoded[1] = lookup[uint8(data) % 16];

        return string(encoded);
    }

    function getImage(uint mask, bytes1 alpha, uint MASK_SIZE, uint PIC_SIZE, uint PIXEL_SIZE) internal pure returns (string memory) {
        uint PIXELS_NUMBER = MASK_SIZE ** 2;

        bytes memory svg = abi.encodePacked(
            '<svg xmlns="http://www.w3.org/2000/svg"',
            '  width="', Strings.toString(PIC_SIZE),
            '" height="', Strings.toString(PIC_SIZE),
            '">',
            '<rect x="0" y="0"',
            '  width="', Strings.toString(PIC_SIZE),
            '" height="', Strings.toString(PIC_SIZE),
            '" fill="#000000"',
            '/>'
        );

        for (uint i = 0; i < PIXELS_NUMBER; i++) {
            uint y = i / MASK_SIZE;
            uint x = i % MASK_SIZE;

            string memory color = getRandomColor(i, alpha);

            if (mask & (1 << (PIXELS_NUMBER - 1 - i)) != 0) {
                svg = abi.encodePacked(svg,
                    '<rect x="', Strings.toString(x * PIXEL_SIZE),
                    '" y="', Strings.toString(y * PIXEL_SIZE),
                    '" width="', Strings.toString(PIXEL_SIZE),
                    '" height="', Strings.toString(PIXEL_SIZE),
                    '" fill="#', color,
                    '"/>'
                );
            }
        }

        return string(abi.encodePacked("data:image/svg+xml;base64,", Base64.encode(abi.encodePacked(svg, '</svg>'))));
    }

}
