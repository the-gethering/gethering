// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import {Gethering} from "../src/Gethering.sol";

import {Script} from "forge-std/Script.sol";


contract GetheringScript is Script {
    function setUp() public {}

    function run() public {
        uint256 deployerPrivateKey = vm.envUint("DEPLOYER_PRIVATE_KEY");

        vm.startBroadcast(deployerPrivateKey);

        Gethering gethering = new Gethering();

        vm.stopBroadcast();
    }
}
