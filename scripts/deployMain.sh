#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 <env-file>"
  exit 1
fi

source "$1"

if [ -n "$VERIFY" ]; then
  VERIFY_FLAG="--verify"
else
  VERIFY_FLAG=""
fi

forge create contracts/src/Gethering.sol:Gethering \
    --ignored-error-codes 5667 \
    --gas-limit 10000000 \
    --optimize \
    --optimizer-runs 10000 \
    --rpc-url $FORGE_RPC_URL \
    --private-key $FORGE_PRIVATE_KEY \
    --etherscan-api-key $ETHERSCAN_API_KEY \
    $VERIFY_FLAG \
    --constructor-args "$(grep -oE "0x[0-9a-fA-F]+" deployedMetaAddress.ts)" \
    | tee /dev/tty | grep 'Deployed to:' | awk '{print $3}' | xargs -I {} echo "export const DEPLOYED_ADDRESS = '{}';" > deployedAddress.ts
