#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 <env-file>"
  exit 1
fi

source "$1"

if [ -n "$VERIFY" ]; then
  VERIFY_FLAG="--verify"
else
  VERIFY_FLAG=""
fi

forge create contracts/src/GetheringMeta.sol:GetheringMeta \
    --ignored-error-codes 5667 \
    --optimize \
    --optimizer-runs 10000 \
    --gas-limit 2000000 \
    --rpc-url $FORGE_RPC_URL \
    --private-key $FORGE_PRIVATE_KEY \
    --etherscan-api-key $ETHERSCAN_API_KEY \
    $VERIFY_FLAG \
    | tee /dev/tty | grep 'Deployed to:' | awk '{print $3}' | xargs -I {} echo "export const META_DEPLOYED_ADDRESS = '{}';" > deployedMetaAddress.ts
